# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#
# how do these two variables get the first 2 values of the values array and set them as these two variables
#
#
def find_second_largest(values):
    if len(values) < 2: # checking if the length of values has more than 2 numbers will pass if not returns nothing.
        return None
    largest = max(values[0], values[1]) #grabs the first value of the value array
    second_largest = min(values[1], values[0]) #grabs the second value of the value array
    for num in values[2:]: # for numbers in value array
        if num > largest: # if the number is larger than the first value in value array
            second_largest = largest # second_largest will set the largest variable
            largest = num # largest will set the second largest cuz it changed it or some sht
        elif num > second_largest: # else number is larger than second_largest
            second_largest = num #second largest set equal to num
    return second_largest # return the second largest number

print(find_second_largest([6,2,3,4,7,5]))
