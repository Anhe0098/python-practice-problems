# Write a class that meets these requirements.
#
# Name:       Student
#
class Student:
    def __init__(self, name):
        self.name = name
        self.scores = []
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
    def add_score(self, scores):
        self.scores.append(scores)
#    * get_average()      # Gets the average of the student's scores
#
    def get_average(self):
        total = 0
        if not self.scores:
            return None
        for score in self.scores:
            total += score
        return total // len(self.scores)
# Example:
student = Student("Malik")

print(student.get_average())    # Prints None
student.add_score(80)
print(student.get_average())    # Prints 80
student.add_score(90)
student.add_score(82)
print(student.get_average())    # Prints 84
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.
