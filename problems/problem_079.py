# Write a class that meets these requirements.
#
# Name:       ReceiptItem
#
class ReceiptItem():
    def __init__(self, quantitiy, price):
        self.quantity = quantitiy
        self.price = price

# Required state:
#    * quantity, the amount of the item bought
#    * price, the amount each one of the things cost
#
# Behavior:
#    * get_total()          # Returns the quantity * price
#
    def get_total(self):
        return self.quantity * self.price
# Example:
item = ReceiptItem(10, 3.45)
print(item.get_total())    # Prints 34.5
