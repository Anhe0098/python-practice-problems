# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

# def remove_duplicate_letters(s):
#     result = "".join(dict.fromkeys(s)) # creating a dict using all of they letters as keys. dict cant have duplicate letters. and joins because of empty string
#     return result


s = "abbbcabcabcb"
b = "awdawdasfasgfwfa" #
def remove_duplicate_letters(string_with_duplicates): #parameter s
    new_string = [] #empty string/array
    for letter in string_with_duplicates:#itterates through each letter in s argument
        if letter not in new_string:
            new_string.append(letter)
    print("".join(new_string))
remove_duplicate_letters(b)

#empty string/array will hold letters seen already
#itterate through each letter in str variable
#for each letter if its not in array add it to string
#if it is there dosent do anything
