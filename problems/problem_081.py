# Write four classes that meet these requirements.
#
# Name:       Animal
#
class Animal:
    def __init__(self, number_of_legs, priamry_color):
        self.number_of_legs = number_of_legs
        self.priamry_color = priamry_color
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
    def describe(self):
        self.__class__.__name__ + "has" + str(self.number_of_legs) + "legs and is primarily" + self.priamry_color
# Name:       Dog, inherits from Animal
class dog(Animal):
    def speak(self):
        return "bark!"
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#

#
#
# Name:       Cat, inherits from Animal
class Cat(Animal):
    def speak(self):
        return "Miao!"

#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
class Snake(Animal):
    def speak(self):
        return "Ssssss!"
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"
ani = dog(str(4), 'black')
print(ani.describe(4,"black"))
