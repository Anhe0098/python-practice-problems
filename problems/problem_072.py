# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
class Person:
    def __init__(self, name,hated_list,loved_list):
        self.name = name
        self.hated_list = hated_list
        self.loved_list = loved_list

# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
    def taste(self, food):
        if food in self.hated_list:
            return False
        if food in self.loved_list:
            return True
        else:
            return


# Example:
person = Person("Malik",
                   ["cottage cheese", "sauerkraut"],
                   ["pizza", "schnitzel"])

print(person.taste("lasagna"))     # Prints None, not in either list
print(person.taste("sauerkraut"))  # Prints False, in the hated list
print(person.taste("pizza"))       # Prints True, in the loved list
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.
